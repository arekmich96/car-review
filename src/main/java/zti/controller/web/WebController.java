package zti.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * WebController class is a MVC controller with Basic Requests Mapping
 *
 * @author Arkadiusz Michalik
 */
@Controller
public class WebController{

    /**
     * Login Page
     */
    @GetMapping("/login")
    public String login(){ return "index"; }

    /**
     * Main Panel Page(Home)
     */
    @GetMapping({"/","/index"})
    public String index(){
        return "index";
    }

    /**
     * Registration Page
     */
    @RequestMapping("/registration")
    public String registration() {
        return "registration";
    }

}
