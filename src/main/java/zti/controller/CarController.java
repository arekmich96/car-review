package zti.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import zti.dao.model.Car;
import zti.dao.model.Cuser;
import zti.dao.repository.CarRepository;
import zti.dao.repository.CuserRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * CarController class is a MVC controller responsible for operations with user cars
 *
 * @author Arkadiusz Michalik
 */
@Controller
public class CarController {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CuserRepository cuserRepository;

    /**
     * Method passes user cars to manage them
     */
    @GetMapping("/cars")
    public String cars(Model model){
        Car car = new Car();
        model.addAttribute("car", car);

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<Cuser> actualLoggedUser = cuserRepository.findByEmail(userDetails.getUsername());
        Cuser user = actualLoggedUser.get();

        System.out.println("user = " + user.getEmail());

        /*get all cars from user*/
        List<Car> userCars = new ArrayList<Car>();
        userCars.addAll(carRepository.findByCuser(user));

        model.addAttribute("userCars", userCars);
        return "cars";
    }

    /**
     * Method responsible for adding car to database
     */
    @PostMapping(value = "/cars")
    public String addCar(@ModelAttribute @Valid Car newCar, BindingResult bindingResult, Model model) {
        if (newCar.getMark() != null && newCar.getModel() != null) {
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            Optional<Cuser> actualLoggedUser = cuserRepository.findByEmail(userDetails.getUsername());
            if (actualLoggedUser.isPresent()) {
                newCar.setCuser(actualLoggedUser.get());
                carRepository.save(newCar);
                System.out.println("new car added: " + newCar);
            }
        }
        return "redirect:/cars";
    }
}
