package zti.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zti.exception.ResourceNotFoundException;
import zti.dao.model.Cuser;
import zti.dao.repository.CuserRepository;
import zti.service.RegistrationService;

import javax.validation.Valid;
import java.util.List;

/**
 * CuserController class is a REST controller responsible for operations with user
 *
 * @author Arkadiusz Michalik
 */
@RestController
@RequestMapping("/api")
public class CuserController {

    @Autowired
    CuserRepository cuserRepository;

    @Autowired
    protected RegistrationService registrationService;

    /**
     * Get all user
     */
    @GetMapping("/user")
    public ResponseEntity<?> getAllCusers(){

        //TODO: tutaj powinno byc odwolanie do servisu a nie repository
        List<Cuser> result = cuserRepository.findAll();

        return ResponseEntity.ok(result);
    }

    /**
     *  Create a new user
     */
    @RequestMapping(value = "/user" , method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Cuser> createCuser(@Valid @RequestBody Cuser cuser){
        Cuser savedUser = registrationService.registerUser(cuser);

        return ResponseEntity.status(HttpStatus.OK).body(savedUser);
    }

    /**
     * Get a single user with properly id
     *
     * @param id user id
     */
    @GetMapping("/user/{id}")
    public Cuser getCuserById(@PathVariable(value = "id") Long id) {
        return cuserRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User","id", id));
    }

    /**
     * Update user with properly id
     */
    @PutMapping("user/{id}")
    public @ResponseBody Cuser updateCuser(@PathVariable(value = "id") Long id, @Valid @RequestBody Cuser updateCuser) {
        Cuser cuser =
                cuserRepository.findById(id).orElseThrow( () -> new ResourceNotFoundException("User","id", id));

        cuser.setEmail(updateCuser.getEmail());
        cuser.setPassword(updateCuser.getPassword());
        cuser.setPhone(updateCuser.getPhone());

        cuserRepository.save(cuser);
        return cuser;
    }

    /**
     * Delete user with properly id
     *
     * @param id user id
     */
    @DeleteMapping("user/{id}")
    public ResponseEntity<?> deleteCuser(@PathVariable(value = "id") Long id){
        Cuser cuser = cuserRepository.findById(id).orElseThrow( () -> new ResourceNotFoundException("User","id", id));

        cuserRepository.delete(cuser);
        return ResponseEntity.ok().build();
    }
}
