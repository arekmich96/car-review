package zti.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import zti.dao.dto.VisitDto;
import zti.dao.model.Car;
import zti.dao.model.Cuser;
import zti.dao.model.Visit;
import zti.dao.repository.CarRepository;
import zti.dao.repository.CuserRepository;
import zti.dao.repository.VisitRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * CalendarController class is a MVC controller responsible for varius operations on Visits Calendar
 *
 * @author Arkadiusz Michalik
 */
@Controller
public class CalendarController {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CuserRepository cuserRepository;

    @Autowired
    private VisitRepository visitRepository;


    /**
     * Method pass attributes to client with user name, user car and empty VisitDTO objecjt to plan the visit
     *
     * @param model model pass to client side
     */
    @GetMapping("/calendar")
    public String calendar(Model model) {

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<Cuser> actualLoggedUser = cuserRepository.findByEmail(userDetails.getUsername());
        Cuser user = actualLoggedUser.get();

        StringBuilder fist_last_name = new StringBuilder();
        fist_last_name.append(user.getFirstname());
        fist_last_name.append(" ");
        fist_last_name.append(user.getLastname());
        //przekazanie uzytkownika
        model.addAttribute("user", fist_last_name);


        /*get all cars from user*/
        List<Car> userCars = new ArrayList<Car>();
        userCars.addAll(carRepository.findByCuser(user));

        model.addAttribute("userCars", userCars);

        //Przekazanie obiektu Wizyty do umówienia się
        VisitDto visitDto = new VisitDto();
        model.addAttribute("addVisit", visitDto);

        return "calendar";
    }
    /**
     * Method responsible for adding visit
     * @param newVisitdto visitDTO object with properly data to save to Visit entity
     */
    @PostMapping("/calendar")
    public String addVisit(@ModelAttribute @Valid VisitDto newVisitdto, BindingResult bindingResult, Model model) {

        //TODO: sprawdzanie czy termin wizyty nie jest juz zajety

        if (newVisitdto.getVisitDate() != null ) {
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            Optional<Cuser> actualLoggedUser = cuserRepository.findByEmail(userDetails.getUsername());
            if (actualLoggedUser.isPresent()) {

                //ustawienie uzytkownika do nowo utworzonej wizyty
                Visit visit = new Visit();
                visit.setCuser(actualLoggedUser.get());

                //znalezienie wybranego pojazdu do przegladu samochodu
                Optional<Car> carforVisit = carRepository.findById(newVisitdto.getCarId());
                visit.setCar(carforVisit.get());

                //ustawienie daty wizyty
                visit.setVisitDate(newVisitdto.getVisitDate());

                visitRepository.save(visit);
                System.out.println("Visit added: " + visit);
            }
        }
        return "redirect:/calendar";
    }


}
