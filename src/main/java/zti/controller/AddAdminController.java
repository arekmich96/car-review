package zti.controller;

import org.codehaus.groovy.runtime.StringGroovyMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import zti.dao.dto.VisitDto;
import zti.dao.model.Cuser;
import zti.dao.model.Role;
import zti.dao.repository.CuserRepository;
import zti.dao.repository.RoleRepository;
import zti.service.CuserService;
import zti.service.RegistrationService;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * AddAdminController class is a controller responsible for add admin by administrator
 *
 * @author Arkadiusz Michalik
 */
@Controller
@RequestMapping("/admin")
public class AddAdminController {

    @Autowired
    private CuserRepository cuserRepository;
    @Autowired
    protected RoleRepository roleRepository;
    @Autowired
    protected RegistrationService registrationService;
    @Autowired
    protected CuserService cuserService;

    /**
     * Get all user
     */
    @GetMapping("/user")
    public String getAllUser(Model model){

        Role roleadmin = roleRepository.findByName("ROLE_ADMIN");

        List<Cuser> users = cuserRepository.findAll();
        List<Cuser> onlyUsers =
                users.stream().filter(user -> !user.getRoles().contains(roleadmin)).collect(Collectors.toList());
        model.addAttribute("users", onlyUsers);

        Cuser cuser = new Cuser();
        model.addAttribute("user", cuser);

        return "adminSelectUser";
    }


    @PostMapping("/add")
    public String addAdmin(@ModelAttribute @Valid Cuser user, BindingResult bindingResult, Model model){
        registrationService.registerAdmin(user.getEmail());
        return "redirect:/admin/user";
    }

    @PostMapping("/delete/user")
    public String deleteUser(@ModelAttribute @Valid Cuser user, BindingResult bindingResult, Model model){
        cuserService.deleteUser(user.getEmail());
        return "redirect:/admin/user";
    }

}
