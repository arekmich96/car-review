package zti.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zti.dao.model.Visit;
import zti.dao.repository.VisitRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * CalendarEventController class is a REST controller responsible for operations on Calendar Events
 *
 * @author Arkadiusz Michalik
 */
@RestController
@RequestMapping("/calendar")
public class CalendarEventController {

    @Autowired
    private VisitRepository visitRepository;

    /**
     * Method passes all visits to client for adding them to calendar
     */
    @GetMapping(value = "/visit" , produces =  MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllVisits(){

        List<Visit> visits = visitRepository.findAll();
        List<HashMap<String, String>> jsonVisits = new ArrayList<>();

        //Create json object with events
        for (Visit visit : visits) {
            HashMap<String, String> map = new HashMap<>();
            map.put("id", visit.getId().toString());
            map.put("user", visit.getCuser().getEmail());
            map.put("car", visit.getCar().getMark() + " " + visit.getCar().getModel());
            map.put("visitDate", visit.getVisitDate().toString());

            jsonVisits.add(map);
        }

        return ResponseEntity.ok(jsonVisits);
    }
}
