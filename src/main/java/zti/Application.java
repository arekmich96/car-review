package zti;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import zti.service.RegistrationService;


/**
 * Spring Boot Application
 *
 * Aplikacja dostępna w chmurze Heroku (https://diagnostic-station.herokuapp.com/)
 * @author Arkadiusz Michalik
 * @version 1.0
 */
@SpringBootApplication
public class Application{

    /**
     * Main application method which run Spring Application - Diagnostic Station
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
