package zti.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import zti.dao.model.Visit;

/**
 * VisitRepository interface with Spring Data JPA methods
 *
 * @author Arkadiusz Michalik
 */
public interface VisitRepository extends JpaRepository<Visit,Long> {
}
