package zti.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zti.dao.model.Cuser;

import java.util.List;
import java.util.Optional;

/**
 * CuserRepository interface with Spring Data JPA methods
 *
 * @author Arkadiusz Michalik
 */
@Repository
public interface CuserRepository extends JpaRepository<Cuser, Long> {

    /**
     * Find user by email adress
     *
     * @param email email adress
     */
    Optional<Cuser> findByEmail(String email);

    Cuser findByCarList(Long carId);

    List<Cuser> findByFirstnameAndLastname(String fistName, String lastName);

    List<Cuser> findByFirstnameOrEmail(String firstName, String email);
}
