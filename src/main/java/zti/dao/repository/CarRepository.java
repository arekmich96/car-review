package zti.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import zti.dao.model.Car;
import zti.dao.model.Cuser;

import java.util.List;

/**
 * CarRepository interface with Spring Data JPA methods
 *
 * @author Arkadiusz Michalik
 */
public interface CarRepository extends JpaRepository<Car,Long> {

    /**
     * Find cars by user
     *
     * @param cuser user details
     */
    List<Car> findByCuser(Cuser cuser);
}
