package zti.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zti.dao.model.Role;

import java.util.Optional;

/**
 * RoleRepository interface with Spring Data JPA methods
 *
 * @author Arkadiusz Michalik
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    /**
     * Find role by id
     *
     * @param aLong role id
     */
    @Override
    Optional<Role> findById(Long aLong);

    /**
     * Find role by role name (ADMIN, USER)
     *
     * @param roleName role name
     */
    Role findByName(String roleName);
}
