package zti.dao.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * persistent_logins entity
 *
 * @author Arkadiusz Michalik
 */
@Getter
@Setter
@Entity
public class persistent_logins {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "persistent_logins_Sequence")
    @SequenceGenerator(name = "persistent_logins_Sequence", sequenceName = "PERSISTENT_LOGINS_SEQ", allocationSize = 1)
    private Long series;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String token;

    @Column(nullable = false)
    private Date last_used;
}
