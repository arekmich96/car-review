package zti.dao.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Car entity
 *
 * @author Arkadiusz Michalik
 */
@Getter
@Setter
@Entity
@Table(name = "Car")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Car implements Serializable {

    @Id
//    @GeneratedValue(strategy=GenerationType.IDENTITY) H2 SQL
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "car_Sequence")
    @SequenceGenerator(name = "car_Sequence", sequenceName = "CAR_SEQ", allocationSize = 1)
    private Long id;

    @NotBlank
    private String mark;

    @NotBlank
    private String model;

    @Column
    private Integer caryear;

    @Column
    private String registration_number;

    @Column
    private Long review_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_cuser_id", nullable = false)
    @JsonBackReference
    private Cuser cuser;

    @OneToMany( cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "car")
    private Set<Visit> visits = new HashSet<>();

    public Car(){}
}
