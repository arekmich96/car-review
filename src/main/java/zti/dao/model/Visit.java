package zti.dao.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Visit entity
 *
 * @author Arkadiusz Michalik
 */
@Getter
@Setter
@Entity
@Table(name = "Visit")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Visit implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "visit_Sequence")
    @SequenceGenerator(name = "visit_Sequence", sequenceName = "VISIT_SEQ", allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_cuser_id", nullable = false)
    private Cuser cuser;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_car_id", nullable = false)
    private Car car;

    @Column
    private Date visitDate;
}
