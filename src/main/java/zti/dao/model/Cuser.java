package zti.dao.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import zti.dao.model.Car;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Cuser entity
 *
 * @author Arkadiusz Michalik
 */
@Getter
@Setter
@Entity
@Table(name = "Cuser")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Cuser implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "cuser_Sequence")
    @SequenceGenerator(name = "cuser_Sequence", sequenceName = "CUSER_SEQ", allocationSize = 1)
    private Long id;

    @Column(nullable=false)
    @NotBlank
    private String firstname;

    @Column(nullable=false)
    @NotBlank
    private String lastname;

    @Column(nullable=false)
    @NotBlank
    private String password;

    @Transient
    private String passwordConfirm;

    @Column(nullable=false, unique=true)
    @Email(message="{errors.invalid_email}")
    @NotBlank
    private String email;

    @NotBlank
    private String phone;

    @OneToMany( cascade = CascadeType.ALL,
                fetch = FetchType.EAGER,
                mappedBy = "cuser")
    @JsonManagedReference
    private List<Car> carList = new ArrayList<>();

    @OneToMany( cascade = CascadeType.ALL,
                fetch = FetchType.EAGER,
                mappedBy = "cuser")
    private Set<Visit> visits = new HashSet<>();


    @ManyToMany(cascade=CascadeType.MERGE)
    //@JsonIgnore
    private Set<Role> roles;

    public Cuser(){}

    public void addCar(Car car) {
        carList.add(car);
        car.setCuser(this);
    }

    public void removeComment(Car car) {
        carList.remove(car);
        car.setCuser(null);
    }
}
