package zti.dao.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Role entity
 *
 * @author Arkadiusz Michalik
 */
@Getter
@Setter
@Entity
@Table(name = "Role")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "role_Sequence")
    @SequenceGenerator(name = "role_Sequence", sequenceName = "ROLE_SEQ", allocationSize = 1)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToMany(mappedBy = "roles")
    //@JsonIgnore
    private Set<Cuser> users;

}
