package zti.dao.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * VisitDto class is data transfer object to get data from client and persist it in Visit entity
 *
 * @author Arkadiusz Michalik
 */
@Data
public class VisitDto {

    private Long userId;
    private Long carId;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date visitDate;
}
