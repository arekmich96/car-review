package zti.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zti.dao.model.Cuser;
import zti.dao.repository.CuserRepository;


import java.util.Collection;

/**
 * CustomUserDetailsService class
 *
 * @author Arkadiusz Michalik
 */
@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private CuserRepository cuserRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Cuser cuser = cuserRepository.findByEmail(userName)
                .orElseThrow(() -> new UsernameNotFoundException("Email " + userName + " not found"));

        return new User(cuser.getEmail(), cuser.getPassword(), getAuthorities(cuser));
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(Cuser cuser) {
        String[] userRoles = cuser.getRoles().stream().map(role -> role.getName()).toArray(String[]::new);
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);

        return authorities;
    }
}
