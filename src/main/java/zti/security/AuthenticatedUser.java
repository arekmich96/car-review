package zti.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import zti.dao.model.Cuser;
import zti.dao.model.Role;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * AuthenticatedUser class
 *
 * @author Arkadiusz Michalik
 */
public class AuthenticatedUser extends User {

    private static final long serialVersionUID = 1L;
    private Cuser cuser;

    public AuthenticatedUser(Cuser cuser)
    {
        super(cuser.getEmail(), cuser.getPassword(), getAuthorities(cuser));
        this.cuser = cuser;
    }

    /**
     * Return actual authenticated user
     */
    public Cuser getUser()
    {
        return cuser;
    }

    /**
     * Method return the collection of roles and permissions assigned to authenticated user
     *
     * @param cuser user
     */
    private static Collection<? extends GrantedAuthority> getAuthorities(Cuser cuser)
    {
        Set<String> roleAndPermissions = new HashSet<>();
        Set<Role> roles = cuser.getRoles();

        for (Role role : roles)
        {
            roleAndPermissions.add(role.getName());
        }
        String[] roleNames = new String[roleAndPermissions.size()];
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(roleAndPermissions.toArray(roleNames));
        return authorities;
    }
}
