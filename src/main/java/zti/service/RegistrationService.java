package zti.service;

import org.springframework.stereotype.Service;
import zti.dao.model.Cuser;

import java.util.List;

/**
 * RegistrationService interface provide service for business operation with user registration
 *
 * @author Arkadiusz Michalik
 */
public interface RegistrationService {
    /**
     * Dodawanie użytkownika do bazy podczas procesu rejestracji
     *
     * @param cuser użytkownik do zarejestrowania w bazie
     * @return zwracany użytkownik dodany do bazy
     */
    Cuser registerUser(Cuser cuser);

    Cuser registerAdmin(String email);
}
