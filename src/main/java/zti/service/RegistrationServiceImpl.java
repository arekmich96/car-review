package zti.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import zti.dao.model.Car;
import zti.dao.model.Cuser;
import zti.dao.model.Role;
import zti.dao.repository.CarRepository;
import zti.dao.repository.CuserRepository;
import zti.dao.repository.RoleRepository;

import java.util.*;

/**
 * RegistrationServiceImpl class provide service for business operation with user registration
 *
 * @author Arkadiusz Michalik
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private CarRepository carRepository;
    @Autowired
    private CuserRepository cuserRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Cuser registerUser(Cuser cuserDTO) {

        Cuser user = new Cuser();
        user.setFirstname(cuserDTO.getFirstname());
        user.setLastname(cuserDTO.getLastname());
        user.setPassword(passwordEncoder.encode(cuserDTO.getPassword()));
        user.setEmail(cuserDTO.getEmail());
        user.setPhone(cuserDTO.getPhone());

        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByName("ROLE_USER"));
        user.setRoles(roles);

        Cuser savedUser =  cuserRepository.save(user);
        return savedUser;
    }

    @Override
    public Cuser registerAdmin(String email) {

        Optional<Cuser> user = cuserRepository.findByEmail(email);
        Cuser savedAdmin = null;
        if (user.isPresent()) {
            Cuser admin = user.get();

            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName("ROLE_ADMIN"));
            admin.setRoles(roles);

            savedAdmin = cuserRepository.save(admin);
        }

        return savedAdmin;
    }

    private boolean emailExist(String email) {
        return true;
    }
}
