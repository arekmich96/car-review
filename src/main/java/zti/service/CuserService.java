package zti.service;

import zti.dao.model.Cuser;

/**
 * CuserService interface provide service for business operation on user
 *
 * @author Arkadiusz Michalik
 */
public interface CuserService {

    /**
     * Operation save user
     *
     * @param  user user
     */
    void save(Cuser user);

    /**
     * Operation will find user by email
     *
     * @param email email address
     */
    Cuser findByEmail(String email);

    void deleteUser(String email);
}
