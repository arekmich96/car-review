package zti.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zti.dao.model.Cuser;
import zti.dao.repository.CuserRepository;

import java.util.Optional;

@Service
public class CuserServiceImpl implements CuserService {

    @Autowired
    private CuserRepository cuserRepository;

    @Override
    public void save(Cuser user) {

    }

    @Override
    public Cuser findByEmail(String email) {
        return null;
    }

    @Override
    public void deleteUser(String email) {
        Optional<Cuser> user = cuserRepository.findByEmail(email);
        if (user.isPresent()) {
            Cuser userToDelete = user.get();
            cuserRepository.delete(userToDelete);
            System.out.println("Usunięto użytkownika o loginie: " + email);
        }
    }
}
