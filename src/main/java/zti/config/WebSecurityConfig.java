package zti.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

/**
 * WebSecurityConfig class is responsible for security configuration
 *
 * @author Arkadiusz Michalik
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService customUserDetailsService;

    @Autowired
    private DataSource dataSource;

    /**
     * Bean responsible for password encode/decode
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    /**
     * Method responsible for configuring authorization
     * In addition, it specifies the login panel and main portal panel
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
             http
                    .headers()
                    .frameOptions().sameOrigin()
                .and()
                     .authorizeRequests()
                     .antMatchers("/assets/**", "/webjars/**","/bodybg/**", "/color/**","/css/**").permitAll()
                     .antMatchers("/error/**", "/font-awesome/**","/fonts/**", "/img/**","/js/**").permitAll()
                     .antMatchers("/packages/**", "/plugins/**").permitAll()
                     .antMatchers("/", "/index").permitAll()
                     .antMatchers("/registration").permitAll()
                     .antMatchers("/calendar").permitAll()
                     .antMatchers( "/api/user","/calendar/visit").permitAll()
                     .antMatchers("/admin/**").hasRole("ADMIN")
                     .antMatchers( "/cars").hasRole("USER")
                     .anyRequest().authenticated()
                .and()
                    .formLogin()
                    .loginPage("/index")
                    .defaultSuccessUrl("/index")
                    .failureUrl("/index?error")
                    .permitAll()
                .and()
                    .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/index?logout")
                    .deleteCookies("my-remember-me-cookie")
                    .permitAll()
                .and()
                    .rememberMe()
                    //.key("my-secure-key")
                    .rememberMeCookieName("my-remember-me-cookie")
                    .tokenRepository(persistentTokenRepository())
                    .tokenValiditySeconds(24 * 60 * 60)
                .and()
                    .exceptionHandling()
        ;
    }

    /**
     * Method responsible for persist token
     */
    private PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl tokenRepositoryImpl = new JdbcTokenRepositoryImpl();
        tokenRepositoryImpl.setDataSource(dataSource);
        return tokenRepositoryImpl;
    }
}
