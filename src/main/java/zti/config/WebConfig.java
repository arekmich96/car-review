package zti.config;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.*;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;

/**
 * WebConfig class is responsible for MVC configuration
 * which add Resources and Views to application
 *
 * @author Arkadiusz Michalik
 */
@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    /**
     * Message source for messages.properties
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * Method responsible for adding resources to application
     * @param registry resource registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
                "/webjars/**",
                "/img/**",
                "/css/**",
                "/color/**",
                "/js/**",
                "/assets/**",
                "/bodybg/**",
                "/font-awesome/**",
                "/fonts/**",
                "/plugins/**",
                "/packages/**",
                "/error/**")
                .addResourceLocations(
                        "classpath:/META-INF/resources/webjars/",
                        "classpath:/static/img/",
                        "classpath:/static/css/",
                        "classpath:/static/color/",
                        "classpath:/static/js/",
                        "classpath:/static/assets/",
                        "classpath:/static/bodybg/",
                        "classpath:/static/font-awesome/",
                        "classpath:/static/fonts/",
                        "classpath:/static/plugins/",
                        "classpath:/static/packages/",
                        "classpath:/static/error/");
    }

    /**
     * Method responsible for adding views to application.
     * Important to use Spring MVC with Thymeleaf framework
     * @param registry view registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry)
    {
        registry.addViewController("/").setViewName("/index.html");
        registry.addViewController("/index").setViewName("/index.html");
        registry.addViewController("/admin/user").setViewName("/adminSelectUser.html");
        registry.addViewController("/cars").setViewName("/cars.html");
    }

    /**
     * Method to set messages properties source
     */
    @Override
    public Validator getValidator() {
        LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
        factory.setValidationMessageSource(messageSource);
        return factory;
    }

    @Bean
    public SpringSecurityDialect securityDialect() {
        return new SpringSecurityDialect();
    }

}
