INSERT INTO cuser (ID, email,firstname,lastname, password, phone) VALUES
(0,'admin@gmail.com','adminF','adminL', '$2a$10$hKDVYxLefVHV/vtuPhWD3OigtRyOykRLDdUAp80Z1crSoS1lFqaFS', '567567567'),
(1,'user@gmail.com','userF','userL', '$2a$10$ByIUiNaRfBKSV6urZoBBxe4UbJ/sS6u1ZaPORHF9AtNWAuVPVz1by', '123123123');


INSERT INTO role (id, name) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_ACTUATOR'),
(3, 'ROLE_USER');

insert into car(id,mark, model, caryear, registration_number, review_id, fk_cuser_id)
values (0,'VW','POLO',2006,'KRA 12121',1, 0),
       (1,'BMW','F10',2013,'KWA 13139',2, 1);

insert into cuser_roles(users_id, roles_id) values
(0,1),
(0,3),
(1,3);