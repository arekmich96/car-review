DROP TABLE IF EXISTS cuser CASCADE;
DROP TABLE IF EXISTS car CASCADE;
DROP TABLE IF EXISTS cardreview CASCADE;
DROP TABLE IF EXISTS meil CASCADE;
DROP TABLE IF EXISTS visit CASCADE;


-- USER --
CREATE TABLE cuser(
 id serial PRIMARY KEY,
 firstname VARCHAR (50) NOT NULL,
 lastname VARCHAR (50),
 password VARCHAR (50) NOT NULL,
 email TEXT UNIQUE NOT NULL,
 phone VARCHAR (12) UNIQUE NOT NULL,
 car_id INTEGER NOT NULL
);

-- CAR --
CREATE TABLE car(
 id serial PRIMARY KEY,
 mark VARCHAR (50) NOT NULL,
 model VARCHAR (50) NOT NULL,
 caryear INTEGER NOT NULL,
 review_id INTEGER NOT NULL,
 user_id INTEGER NOT NULL
);

-- CARD REVIEW --
CREATE TABLE cardreview(
 id serial PRIMARY KEY,
 datereview TIMESTAMP NOT NULL,
 dateexpiration TIMESTAMP NOT NULL,
 malfunctions TEXT,
 car_id INTEGER NOT NULL
);

-- MEIL --
CREATE TABLE meil(
 id serial PRIMARY KEY,
 datesend TIMESTAMP NOT NULL,
 user_id INTEGER NOT NULL
);

-- VISIT --
CREATE TABLE visit(
 id serial PRIMARY KEY,
 datesend TIMESTAMP NOT NULL,
 user_id INTEGER NOT NULL,
 car_id INTEGER NOT NULL
);

------------ FOREIGN KEYS ------------
-- FK User-Car --
ALTER TABLE cuser
    ADD CONSTRAINT fk_user_car FOREIGN KEY (car_id) REFERENCES car (id);

-- FK Car-User --
ALTER TABLE car
    ADD CONSTRAINT fk_car_user FOREIGN KEY (user_id) REFERENCES cuser (id);

-- FK Car-CardReview --
ALTER TABLE car
    ADD CONSTRAINT fk_car_review FOREIGN KEY (review_id) REFERENCES cardreview (id);

-- FK CardReview-CAR --
ALTER TABLE cardreview
    ADD CONSTRAINT fk_cardreview_car FOREIGN KEY (car_id) REFERENCES car (id);

-- FK Meil-User --
ALTER TABLE cuser
    ADD CONSTRAINT fk_meil_user FOREIGN KEY (user_id) REFERENCES cuser (id);

-- FK Visit-User --
ALTER TABLE cuser
    ADD CONSTRAINT fk_visit_user FOREIGN KEY (user_id) REFERENCES cuser (id);

-- FK Visit-Car --
ALTER TABLE cuser
    ADD CONSTRAINT fk_visit_car FOREIGN KEY (car_id) REFERENCES car (id);

