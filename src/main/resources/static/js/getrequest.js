$( document ).ready(function() {

    // GET REQUEST
    $("#getAllCustomerId").click(function(event){
        event.preventDefault();
        ajaxGet();
    });

    // DO GET
    function ajaxGet(){
        console.log("window.location.origin + \"/api/user\", = "+window.location.origin + "/api/user");
        $.ajax({
            type : "GET",
            url : window.location.origin + "/api/user",
            success: function(result){

                    $('#getResultDiv ul').empty();
                    var custList = "";
                    $.each(result, function(i, customer){
                        var customer = '- Customer with Id = ' + i + ', firstname = ' + customer.firstname + ', lastName = ' + customer.lastname + '<br>';
                        console.log("Customer = "+customer);
                        $('#getResultDiv .list-group').append(customer);
                    });
                    console.log("Success: ", result);
            },
            error : function(e) {
                $("#getResultDiv").html("<strong>Error</strong>");
                console.log("ERROR: ", e);
            }
        });
    }
});