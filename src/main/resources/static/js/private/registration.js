$( document ).ready(function() {

    //Zabezpieczeniem przed CSRF - Cross-Site Request Forgery
    var token = $("meta[name='_csrf']").attr("content");

    // SUBMIT FORM
    $("#registrationForm").submit(function(event) {
        // Prevent the form from submitting via the browser.
        event.preventDefault();
        ajaxPost();
    });


    function ajaxPost(){
        // PREPARE FORM DATA
        var formDataUser = {
            firstname : $("#first_name").val(),
            lastname :  $("#last_name").val(),
            password: $("#password").val(),
            email : $("#email").val(),
            phone :  $("#phone").val(),
            carList: []
        };

        $.ajax({
            type : "POST",
            headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
            contentType : "application/json",
            url : window.location.origin + "/api/user",
            data : JSON.stringify(formDataUser),
            dataType : 'json',
            success: function (response) {

                console.log("JSON = " + JSON.stringify(response));

                $("#postResultDiv").html(
                    "<div class=\"alert success\">" +
                    "<span class=\"closebtn\">&times;</span>" +
                    "<strong>Gratulacje!</strong> Pomyślnie utworzono konto użytkownika:" +
                    response.firstname + " " + response.lastname +
                    "</div>");

                closeAlert();
            },
            error : function(e) {
                $("#postResultDiv").html("<div class=\"alert\">" +
                    "<span class=\"closebtn\">&times;</span>" +
                    "<strong>Błąd!</strong> Rejestracja użytkownika zakończyła sie niepowodzeniem" +
                    "</div>");

                closeAlert();
            }
        });

        // Reset formDataUser after Posting
        resetData();

    }

    function resetData(){
        $("#first_name").val("");
        $("#last_name").val("");
        $("#password").val("");
        $("#password-check").val("");
        $("#phone").val("");
        $("#email").val("");
        $("#cpatchaTextBox").val("");
    }

    /* Alerts */
    function closeAlert() {
        var close = document.getElementsByClassName("closebtn");
        var i;

        for (i = 0; i < close.length; i++) {
            close[i].onclick = function(){
                var div = this.parentElement;
                div.style.opacity = "0";
                setTimeout(function(){ div.style.display = "none"; }, 600);
            }
        }
    }
});