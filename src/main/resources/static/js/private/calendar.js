document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
        },
        customButtons: {
            addEventButton: {
                text: 'add event...',
                click: function () {
                    var dateStr = prompt('Enter a date in YYYY-MM-DD format');
                    var date = new Date(dateStr + 'T00:00:00'); // will be in local time

                    if (!isNaN(date.valueOf())) { // valid?
                        calendar.addEvent({
                            title: 'dynamic event',
                            start: date,
                            allDay: true
                        });
                        alert('Great. Now, update your database...');
                    } else {
                        alert('Invalid date.');
                    }
                }
            }
        },

        defaultDate: new Date(),
        navLinks: true, // can click day/week names to navigate views

        weekNumbers: true,
        weekNumbersWithinDays: true,
        weekNumberCalculation: 'ISO',

        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [
            {
                title: 'All Day Event',
                start: '2019-06-01'
            },
            {
                title: 'Kontrola Stacji Diagnostycznej',
                start: '2019-06-07',
                end: '2019-06-10'
            },
            {
                groupId: 999,
                title: 'Dzień wolny',
                start: '2019-06-09T16:00:00'
            },
            {
                groupId: 999,
                title: 'Dzień wolny',
                start: '2019-06-16T16:00:00'
            },
            {
                title: 'Konferencja',
                start: '2019-06-11',
                end: '2019-06-13'
            },
            {
                title: 'Spotkanie',
                start: '2019-06-12T10:30:00',
                end: '2019-06-12T12:30:00'
            },
            {
                title: 'Obiad',
                start: '2019-06-12T12:00:00'
            },
            {
                title: 'Spotkanie',
                start: '2019-06-12T14:30:00'
            },
            {
                title: 'Link do Google',
                url: 'http://google.com/',
                start: '2019-06-28'
            },
            // red areas where no events can be dropped
            {
                start: '2019-06-24',
                end: '2019-06-28',
                overlap: false,
                rendering: 'background',
                color: '#ff9f89'
            },
            {
                start: '2019-06-06',
                end: '2019-06-08',
                overlap: false,
                rendering: 'background',
                color: '#ff9f89'
            }
        ],
        selectable: true,
        selectHelper: true,
     /*   eventRender: function(event, element){
            $(element).on("click", function () {
                alert("Hover!");
                $("#startTime").html(event.start);
                $("#endTime").html(event.end);
                $("#eventInfo").html(event.description);
                $("#eventLink").attr('href', event.url);
                $("#eventContent").adjustDialog({ modal: true, title: event.title, width:350});
            });
        },*/
        select: function(start, end, allDay) {
            var title = prompt('Dodaj wydarzenie do kalendarza');
            if (title) {
                calendar.fullCalendar('renderEvent',
                    {
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay
                    },
                    true // make the event "stick"
                );
            }
            calendar.fullCalendar('unselect');
        },
        eventClick: function(info) {
            $("#startTime").html(info.event.start);
            $("#endTime").html(info.event.end);
            $("#eventInfo").html(info.event.description);
            $("#eventLink").attr('href', info.event.url);
            $("#eventContent").dialog({ modal: true, title: info.event.title, width:350});
        }
    });

    calendar.render();
    ajaxGetCalendarEvent();

    //Zabezpieczeniem przed CSRF - Cross-Site Request Forgery
    var token = $("meta[name='_csrf']").attr("content");

    function ajaxGetCalendarEvent() {

        $.ajax({
            headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
            url: window.location.origin + "/calendar/visit",
            dataType: 'json',
            success: function (response) {
                console.log("Success JSON = " + JSON.stringify(response));

                //TODO: dodawanie wizyt do kalendarza !!!
                for (var key in response) {
                    if (response.hasOwnProperty(key)) {
                        console.log(response[key].id);

                        var event = {
                            id: response[key].id,
                            title: response[key].user + ': ' + response[key].car,
                            start: response[key].visitDate
                        };
                        calendar.addEvent(event);
                    }
                }
            },
            error: function (e) {
                console.log("Error JSON = " + JSON.stringify(e));
            }
        });

    }

});








